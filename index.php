<html lang="kr">
    <head>
        <meta charset="utf-8">
        <title>아킬라미디어. 세상의 모든 지식을 책으로！</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="http://bootstraptaste.com" />
        <!-- css -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link href="css/fancybox/jquery.fancybox.css" rel="stylesheet">
        <!--<link href="css/jcarousel.css" rel="stylesheet" />-->
        <link href="css/flexslider.css" rel="stylesheet" />
        <link href="css/style.css" rel="stylesheet" />
        <link href="skins/default.css" rel="stylesheet" />

    </head>
    <body>
        <div id="wrapper">
            <!-- start header 모든 페이지 공통 active만 페이지 별로 별도．　-->　

            <header>
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html"><img src="img/aquila.png"></a>
                        </div>
                        <div class="navbar-collapse collapse ">
                            <ul class="nav navbar-nav">
                                <li class="active">
                                    <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">아킬라미디어<b class=" icon-angle-down"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="intro.html">아킬라미디어 소개</a></li>
                                        <li><a href="chief.html">대표 인사말</a></li>
                                        <li><a href="profile.html">연혁</a></li>
                                        <li><a href="map.html">오시는 길</a></li>
                                    </ul>
                                </li>
                                <li><a href="portfolio.html">도서소개</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">미디어콘텐츠<b class=" icon-angle-down"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="media.html">미디어콘텐츠 소개</a></li>
                                        <li><a href="replay.html">의료통신 다시보기</a></li>
                                        <li><a href="comment.html">의료통신 시청후기</a></li>
                                    </ul>
                                </li>
                                <li><a href="join.html">전자책／도서저자＆원고모집</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">유료회원<b class=" icon-angle-down"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="typography.html">유료회원모집</a></li>
                                        <li><a href="fighting.html">응원의 한마디</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <!-- end header -->

            <section id="featured">
                <!-- start slider -->
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Slider -->
                            <div id="main-slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <img src="img/slides/1.jpg" alt="" />
                                    </li>
                                    <li>
                                        <img src="img/slides/2.jpg" alt="" />
                                    </li>
                                    <li>
                                        <img src="img/slides/3.jpg" alt="" />
                                    </li>
                                </ul>
                            </div>
                            <!-- end slider -->
                        </div>
                    </div>
                </div>	

            </section>
            <section class="callaction">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="big-cta">
                                <div class="cta-text ">
                                    <img src="img/main1.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="box">
                                        <div class="box-gray aligncenter">
                                            <h5>2013년 전반기 종합 의정보고서</h5>
                                            <div class="icon">
                                                <i class="fa fa-desktop fa-3x"></i>
                                            </div>
                                            <p>
                                                안녕하십니까. [위버멘쉬소프트]입니다. 변함없는 고객님의 성원에 감사드립니다.[위버멘쉬소프트]는 ‘고객의 입장에서 고객이 만족할 때까지’ 라는경영이념을 모티브로 항상 고객님의 행복을 위해 최선을 다하겠습니다. [위버멘쉬소프트]는 웹솔루션 개발 및 인터넷비지니스모델 ...
                                            </p>
                                            <p>
                                                2013-01-28
                                            </p>

                                        </div>
                                        <div class="box-bottom">
                                            <a href="#">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="box">
                                        <div class="box-gray aligncenter">
                                            <h5>2013년 신년 의정보고서</h5>
                                            <div class="icon">
                                                <i class="fa fa-pagelines fa-3x"></i>
                                            </div>
                                            <p>
                                                안녕하십니까. [위버멘쉬소프트]입니다. 변함없는 고객님의 성원에 감사드립니다.[위버멘쉬소프트]는 ‘고객의 입장에서 고객이 만족할 때까지’ 라는경영이념을 모티브로 항상 고객님의 행복을 위해 최선을 다하겠습니다. [위버멘쉬소프트]는 웹솔루션 개발 및 인터넷비지니스모델 ...
                                            </p>
                                            <p>
                                                2013-01-28
                                            </p>

                                        </div>
                                        <div class="box-bottom">
                                            <a href="#">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="box">
                                        <div class="box-gray aligncenter">
                                            <h5>2012년 후반기 종합 의정보고서</h5>
                                            <div class="icon">
                                                <i class="fa fa-edit fa-3x"></i>
                                            </div>
                                            <p>
                                                안녕하십니까. [위버멘쉬소프트]입니다. 변함없는 고객님의 성원에 감사드립니다.[위버멘쉬소프트]는 ‘고객의 입장에서 고객이 만족할 때까지’ 라는경영이념을 모티브로 항상 고객님의 행복을 위해 최선을 다하겠습니다. [위버멘쉬소프트]는 웹솔루션 개발 및 인터넷비지니스모델 ...
                                            </p>
                                            <p>
                                                2013-01-28
                                            </p>

                                        </div>
                                        <div class="box-bottom">
                                            <a href="#">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="box">
                                        <div class="box-gray aligncenter">
                                            <h5>2012년 추석 의정보고서</h5>
                                            <div class="icon">
                                                <i class="fa fa-code fa-3x"></i>
                                            </div>
                                            <p>
                                                안녕하십니까. [위버멘쉬소프트]입니다. 변함없는 고객님의 성원에 감사드립니다.[위버멘쉬소프트]는 ‘고객의 입장에서 고객이 만족할 때까지’ 라는경영이념을 모티브로 항상 고객님의 행복을 위해 최선을 다하겠습니다.[위버멘쉬소프트]는 웹솔루션 개발 및 인터넷비지니스모델 ...
                                            </p>
                                            <p>
                                                2013-01-28
                                            </p>
                                        </div>
                                        <div class="box-bottom">
                                            <a href="#">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- divider -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="solidline">
                            </div>
                        </div>
                    </div>
                    <!-- end divider -->
                    <!-- Portfolio Projects -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!--<h4 class="heading">Recent Works</h4>-->
                            <div class="row">
                                <section id="projects">
                                    <ul id="thumbs" class="portfolio">
                                        <!-- Item Project and Filter Name -->
                                        <li class="col-lg-3 design" data-id="id-0" data-type="web">
                                            <div class="item-thumbs">
                                                <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                                <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Work 1" href="img/works/1.jpg">
                                                    <span class="overlay-img"><br><br><p><b>[최대집의 의료통신]</b>과 함께하는 의료개혁의 동반자가 되어주시기 바랍니다!!</p></span>
                                                    <!--<span class="overlay-img-thumb font-icon-plus"></span>-->
                                                </a>
                                                <!-- Thumb Image and Description -->
                                                <img src="img/works/1.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
                                            </div>
                                        </li>
                                        <!-- End Item Project -->
                                        <!-- Item Project and Filter Name -->
                                        <li class="item-thumbs col-lg-3 design" data-id="id-1" data-type="icon">
                                            <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                            <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Work 2" href="img/works/2.jpg">
                                                <span class="overlay-img"><br><p><b></t>　의료통신 시청후기</b></p><br><br><p>시청후기를 남겨 주세요．</p></span>
                                                <!--<span class="overlay-img-thumb font-icon-plus"></span>-->
                                            </a>
                                            <!-- Thumb Image and Description -->
                                            <img src="img/works/2.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
                                        </li>
                                        <!-- End Item Project -->
                                        <!-- Item Project and Filter Name -->
                                        <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="illustrator">
                                            <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                            <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Work 3" href="img/works/3.jpg">
                                                <span class="overlay-img"><br><p><b>　　　오시는길</b></p><br><br><p> 방문은 언제나 환영입니다．</p></span>
                                                <!--<span class="overlay-img-thumb font-icon-plus"></span>-->
                                            </a>
                                            <!-- Thumb Image and Description -->
                                            <img src="img/works/3.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
                                        </li>
                                        <!-- End Item Project -->
                                        <!-- Item Project and Filter Name -->
                                        <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="illustrator">
                                            <!-- Fancybox - Gallery Enabled - Title - Full Image -->
                                            <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="Work 4" href="img/works/4.jpg">
                                                <span class="overlay-img"><br><p><b>＂의료혁신투쟁위원회＂</b></p><br><p>의사와 국민 모두에게 <br>정의로운 의료제도 확립을 위해 노력하겠습니다．</p></span>
                                                <!--<span class="overlay-img-thumb font-icon-plus"></span>-->
                                            </a>
                                            <!-- Thumb Image and Description -->
                                            <img src="img/works/4.jpg" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis elementum odio. Curabitur pellentesque, dolor vel pharetra mollis.">
                                        </li>
                                        <!-- End Item Project -->
                                    </ul>
                                </section>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
　<!--／／／／／／／／／／／／／／／／／／／／／모든　페이지　공통　사항／／／／／／／／／／／／／／／／／／／／／／-->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="widget">
                                <h4 class="widgetheading">Get in touch with us</h4>
                                <address>
                                    <strong>Moderna company Inc</strong><br>
                                    Modernbuilding suite V124, AB 01<br>
                                    Someplace 16425 Earth </address>
                                <p>
                                    <i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
                                    <i class="icon-envelope-alt"></i> email@domainname.com
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="widget">
                                <h4 class="widgetheading">의료통신 응원메세지</h4>
                                <ul class="link-list">
                                    <li><a href="#"><b>［성진우］ 안녕하세요. 의원님께 의지하고 있는 한사람 입니다.부디 오래오래 건강하시고 국민들을 위해 열심히 일해주세요.</b></a></li>
                                    <li></li>
                                    <li><a href="#">［위버멘쉬］ 홍길동 의원님 많이 힘드시지요? 힘드시겠지만 쓰러지시면 안됩니다. 홍길동 의원님의 뒤에는 국민들이 있습니다.</a></li>
                                    <li></li>
                                    <li><a href="#"><b>［백두산］ 홍길동 의원님 많이 힘드시지요?힘드시겠지만 쓰러지시면 안됩니다. 홍길동 의원님의 뒤에는 국민들이 있습니다.</b></a></li>
                                </ul>
                            </div>
                        </div>


                        <div class="col-lg-3">
                            <div class="widget">
                                <h4 class="widgetheading">의료통신 포토갤러리</h4>
                                <div class="flickr_badge">
                                    <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sub-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="copyright">
                                    <p>
                                        <span>&copy; Moderna 2014 All right reserved. By </span><a href="http://bootstraptaste.com" target="_blank">Bootstrap Themes</a>
                                    </p>
                                    <!-- 
                                        All links in the footer should remain intact. 
                                        Licenseing information is available at: http://bootstraptaste.com/license/
                                        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Moderna
                                    -->
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <ul class="social-network">
                                    <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
        <!-- javascript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.fancybox.pack.js"></script>
        <script src="js/jquery.fancybox-media.js"></script>
        <script src="js/google-code-prettify/prettify.js"></script>
        <script src="js/portfolio/jquery.quicksand.js"></script>
        <script src="js/portfolio/setting.js"></script>
        <script src="js/jquery.flexslider.js"></script>
        <script src="js/animate.js"></script>
        <script src="js/custom.js"></script>
    </body>
</html>